DROP TABLE Proposition;
DROP TABLE Voeu;
DROP TABLE Etablissement;
DROP TABLE Eleve;
DROP TABLE Mention;
DROP TABLE Filiere;

CREATE TABLE Filiere(

	nomFiliere VARCHAR2(30)
		CONSTRAINT pk_Filiere PRIMARY KEY,

	estSelective NUMBER(1)
		CONSTRAINT nn_Filiere NOT NULL
		CONSTRAINT ck_estSelective CHECK (estSelective IN (0,1))
);

CREATE TABLE Mention(

	intituleMention VARCHAR2(30),

	uneFiliere VARCHAR2(30)
		CONSTRAINT fk_Mention_Filiere REFERENCES Filiere(nomFiliere),

	CONSTRAINT pk_Mention PRIMARY KEY (intituleMention, uneFiliere)
);

CREATE TABLE Eleve(

	numeroINE VARCHAR2(30)
		CONSTRAINT pk_Eleve PRIMARY KEY,

	nom VARCHAR2(30),

	prenom VARCHAR2(30),

	age NUMBER(3)
		CONSTRAINT ck_age CHECK (age >= 0),

	estBoursier NUMBER(1)
		CONSTRAINT ck_estBoursier CHECK (estBoursier IN (0,1)),

	voie VARCHAR2(30)
		CONSTRAINT ck_Eleve CHECK (voie IN ('Générale','Technologique','Professionnelle')),

	CONSTRAINT uq_nom_prenom UNIQUE (nom, prenom)

);

CREATE TABLE Etablissement(

	numEtablissement NUMBER
		CONSTRAINT pk_Etablissement PRIMARY KEY,

	estPrive NUMBER(1)
		CONSTRAINT ck_estPrive CHECK (estPrive IN (0,1))

);

CREATE TABLE Voeu(

	numeroVoeu VARCHAR2(30)
		CONSTRAINT pk_Voeu PRIMARY KEY,

	resultat VARCHAR2(30)
		CONSTRAINT ck_Voeu CHECK (resultat IN ('Non Classé','Classé Non Appelé','Classé Appelé')),

	dateAppel DATE,

	dateAcceptation DATE,

	unEleve VARCHAR2(30)
		CONSTRAINT fk_Voeu_Eleve REFERENCES Eleve(numeroINE)
		CONSTRAINT nn_unEleve NOT NULL,

	unEtablissementVoeu NUMBER
		CONSTRAINT fk_Voeu_Etablissement REFERENCES Etablissement(numEtablissement)
		CONSTRAINT nn_unEtablissementVoeu NOT NULL,

	uneMentionVoeu VARCHAR2(30)
		CONSTRAINT nn_uneMentionVoeu NOT NULL,

	uneFiliereVoeu VARCHAR2(30)
		CONSTRAINT nn_uneFiliereVoeu NOT NULL,

	CONSTRAINT fk_Voeu_Mention FOREIGN KEY (uneMentionVoeu, uneFiliereVoeu) REFERENCES Mention(intituleMention, uneFiliere)

);

CREATE TABLE Proposition(

	unEtablissementP NUMBER
		CONSTRAINT fk_Proposition_Etablissement REFERENCES Etablissement(numEtablissement),

	uneMentionP VARCHAR2(30)
		CONSTRAINT nn_uneMentionP NOT NULL,

	uneFiliereP VARCHAR2(30)
		CONSTRAINT nn_uneFiliereP NOT NULL,

	CONSTRAINT fk_Proposition_Mention FOREIGN KEY (uneMentionP, uneFiliereP) REFERENCES Mention(intituleMention, uneFiliere),

	CONSTRAINT pk_Proposition PRIMARY KEY (unEtablissementP, uneMentionP, uneFiliereP)

);

INSERT INTO Eleve VALUES ('1', 'nom1', 'prenom1', '19', 1, 'Générale');
INSERT INTO Eleve VALUES ('2', 'nom2', 'prenom2', '16', 0, 'Générale');

INSERT INTO Filiere VALUES ('BUT', 1);
INSERT INTO Filiere VALUES ('FAC', 0);

INSERT INTO Mention VALUES ('Info', 'BUT');
INSERT INTO Mention VALUES ('Bio', 'FAC');

INSERT INTO Etablissement VALUES (1, 0);
INSERT INTO Etablissement VALUES (2, 1);

INSERT INTO Voeu VALUES (1, 'Classé Appelé', NULL, NULL, '1', 1, 'Info', 'BUT');
INSERT INTO Voeu VALUES (2, 'Classé Non Appelé', NULL, NULL, '2', 1, 'Bio', 'FAC');