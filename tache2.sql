-- Existence de la clé primaire : nomFiliere, numeroINE

INSERT INTO Filiere VALUES ('Informatique', 1);
INSERT INTO Filiere VALUES ('Informatique', 1);
-- Erreur, le 'nomFiliere' existe déjà

INSERT INTO Mention VALUES ('bien', 'Informatique');
INSERT INTO Mention VALUES ('bien', 'Informatique');
-- erreur, le 'intituleMention' existe déjà

-- unicit2 de la clé primaire : numeroVoeu

INSERT INTO Eleve(numeroINE) VALUES ('1');
INSERT INTO Etablissement(numEtablissement) VALUES ('1');

INSERT INTO Voeu(numeroVoeu, unEleve, unEtablissementVoeu, uneMentionVoeu, uneFiliereVoeu) VALUES ('1', '1', '1', 'bien', 'Informatique');
INSERT INTO Voeu(numeroVoeu, unEleve, unEtablissementVoeu, uneMentionVoeu, uneFiliereVoeu) VALUES ('1', '1', '1', 'bien', 'Informatique');

-- unicité de la clé candidate : (nom,prenom)

INSERT INTO Eleve(numeroINE, nom, prenom) VALUES ('2', 'ewen', 'belbeoch');
INSERT INTO Eleve(numeroINE, nom, prenom) VALUES ('3', 'ewen', 'belbeoch');

-- integrité référentielle de la clé  ́etrangère

-- test 1
INSERT INTO Mention VALUES ('test1', 'Informatique');
-- test 2
INSERT INTO Filiere VALUES ('Informatique', 1);
INSERT INTO Mention VALUES ('test1', 'Informatique');
DELETE FROM Filiere WHERE nomFiliere = 'Informatique';

-- CHECK de valeurs positives de l’attribut age

INSERT INTO Eleve(numeroINE, nom, prenom, age) VALUES ('5', 'toto', 'belbeoch', -1);

-- CHECK de DOM de l’attribut voie

INSERT INTO Eleve(numeroINE, nom, prenom, voie) VALUES ('6', 'toto2', 'belbeoch', 'inconnu');
