-- Suppression des tables pour réutilisation du script

DROP TABLE Proposition;
DROP TABLE Voeu;
DROP TABLE Etablissement;
DROP TABLE Eleve;
DROP TABLE Mention;
DROP TABLE Filiere;

-- Création de la table Filiere

CREATE TABLE Filiere(

	nomFiliere VARCHAR2(30)
		CONSTRAINT pk_Filiere PRIMARY KEY,

	estSelective NUMBER(1)
		CONSTRAINT nn_Filiere NOT NULL
		CONSTRAINT ck_estSelective CHECK (estSelective IN (0,1))
);

-- Création de la table Mention

CREATE TABLE Mention(

	intituleMention VARCHAR2(30),

	uneFiliere VARCHAR2(30)
		CONSTRAINT fk_Mention_Filiere REFERENCES Filiere(nomFiliere),

	CONSTRAINT pk_Mention PRIMARY KEY (intituleMention, uneFiliere)
);

-- Création de la table Eleve

CREATE TABLE Eleve(

	numeroINE VARCHAR2(30)
		CONSTRAINT pk_Eleve PRIMARY KEY,

	nom VARCHAR2(30)
		CONSTRAINT nn_nom NOT NULL,

	prenom VARCHAR2(30)
		CONSTRAINT nn_prenom NOT NULL,

	age NUMBER(3)
		CONSTRAINT ck_age CHECK (age >= 0),

	estBoursier NUMBER(1)
		CONSTRAINT ck_estBoursier CHECK (estBoursier IN (0,1)),

	voie VARCHAR2(30)
		CONSTRAINT ck_Eleve CHECK (voie IN ('Générale','Technologique','Professionnelle')),

	CONSTRAINT uq_nom_prenom UNIQUE (nom, prenom)

);

-- Création de la table Etablissement

CREATE TABLE Etablissement(

	numEtablissement NUMBER
		CONSTRAINT pk_Etablissement PRIMARY KEY,

	estPrive NUMBER(1)
		CONSTRAINT ck_estPrive CHECK (estPrive IN (0,1))

);

-- Création de la table Voeu

CREATE TABLE Voeu(

	numeroVoeu VARCHAR2(30)
		CONSTRAINT pk_Voeu PRIMARY KEY,

	resultat VARCHAR2(30)
		CONSTRAINT ck_Voeu CHECK (resultat IN ('Non Classé','Classé Non Appelé','Classé Appelé')),

	dateAppel DATE,

	dateAcceptation DATE,

	unEleve VARCHAR2(30)
		CONSTRAINT fk_Voeu_Eleve REFERENCES Eleve(numeroINE)
		CONSTRAINT nn_unEleve NOT NULL,

	unEtablissementVoeu NUMBER
		CONSTRAINT fk_Voeu_Etablissement REFERENCES Etablissement(numEtablissement)
		CONSTRAINT nn_unEtablissementVoeu NOT NULL,

	uneMentionVoeu VARCHAR2(30)
		CONSTRAINT nn_uneMentionVoeu NOT NULL,

	uneFiliereVoeu VARCHAR2(30)
		CONSTRAINT nn_uneFiliereVoeu NOT NULL,

	CONSTRAINT fk_Voeu_Mention FOREIGN KEY (uneMentionVoeu, uneFiliereVoeu) REFERENCES Mention(intituleMention, uneFiliere)

);

-- Création de la table Proposition

CREATE TABLE Proposition(

	unEtablissementP NUMBER
		CONSTRAINT fk_Proposition_Etablissement REFERENCES Etablissement(numEtablissement),

	uneMentionP VARCHAR2(30)
		CONSTRAINT nn_uneMentionP NOT NULL,

	uneFiliereP VARCHAR2(30)
		CONSTRAINT nn_uneFiliereP NOT NULL,

	CONSTRAINT fk_Proposition_Mention FOREIGN KEY (uneMentionP, uneFiliereP) REFERENCES Mention(intituleMention, uneFiliere),

	CONSTRAINT pk_Proposition PRIMARY KEY (unEtablissementP, uneMentionP, uneFiliereP)

);

-- ajouter une colonne dans une table
ALTER TABLE Etablissement ADD departement NUMBER;

-- supprimer la colonne ajoutée
ALTER TABLE Etablissement DROP COLUMN departement;

-- ajouter d’une contrainte
ALTER TABLE Eleve MODIFY age NUMBER CONSTRAINT nn_age NOT NULL;

-- supprimer la contrainte ajoutée.
ALTER TABLE Eleve DROP CONSTRAINT nn_age; 
