-- 1 : Projection avec restriction

	-- 1.1 - Question : Quels sont les noms et prénoms des élèves qui ont plus de 18 ans ?

	-- Réponse en algèbre relationnelle :

	-- Eleve{age > 18}[nom, prenom]

	-- Réponse en SQL :

	SELECT DISTINCT UPPER(nom), UPPER(prenom) 
	FROM Eleve 
	WHERE age > 18;

	-- 1.2 - Question : Quelles sont les mentions issues de filières séléctives ?

	-- Réponse en algèbre relationnelle :

	-- Eleve{}

	-- Réponse en SQL :

	SELECT DISTINCT UPPER(intituleMention)
	FROM Mention
	WHERE uneFiliere IN(
		SELECT DISTINCT UPPER(nomFiliere)
		FROM Filiere
		WHERE estSelective = 1
	);

-- 2 : Union, intersection, différence ensembliste

	-- 2.1 - Question : Quels sont les noms des filieres et des mentions ?

	-- Réponse en algèbre relationnelle :

	-- Filiere[nomFiliere] U Mention[intituleMention]

	-- Réponse en SQL :

	SELECT DISTINCT UPPER(nomFiliere)
	FROM Filiere
	UNION
	SELECT DISTINCT UPPER(intituleMention)
	FROM Mention;

	-- 2.2 - Question : Quels sont les numéros INE des élèves qui ont plus de 18 ans et qui s'appellent Thibault ?

	-- Réponse en algèbre relationnelle :

	-- Eleve{prenom = 'Thibault'}[numeroINE] n Eleve{age > 18}[numeroINE]

	-- Réponse en SQL :

	SELECT DISTINCT numeroINE
	FROM Eleve
	WHERE prenom = 'Thibault'
	INTERSECT
	SELECT DISTINCT numeroINE
	FROM Eleve
	WHERE age > 18;

	-- 2.3 - Question :

    -- afficher tous les nom d'élèves qui ne sont pas Belbeoch

    -- Réponse en algèbre relationnelle :

    -- Eleve[nom] - Eleve{nom = 'Belbeoch'}[nom]

    -- Réponse en SQL :

    SELECT DISTINCT nom
    FROM Eleve
    MINUS
    SELECT DISTINCT nom
    FROM Eleve
    WHERE UPPER(nom) = 'Belbeoch';

-- 3 : Tri avec restriction 

	-- 3.1 - Question : Quels sont les noms de familles des élèves qui s'appellent Thomas dans l'ordre alphabétique ?

	-- Réponse en algèbre relationnelle :

	--	Eleve{prenom = 'Thomas'}[nom](nom>)

	-- Réponse en SQL :

	SELECT DISTINCT UPPER(nom)
	FROM Eleve
	WHERE prenom = 'Thomas'
	ORDER BY UPPER(nom);

-- 4 : Tri multi-attributs avec restriction

	-- 4.1 - Question : Quels sont les noms et prénoms des élèves qui ont plus de 18 ans dans l'ordre alphabétique des noms puis des prénoms ?

	-- Réponse en algèbre relationnelle :

	-- Eleve{age > 18}[nom, prenom](nom>)(prenom>)

	-- Réponse en SQL :

	SELECT DISTINCT UPPER(nom), UPPER(prenom)
	FROM Eleve
	WHERE age > 18
	ORDER BY UPPER(nom), UPPER(prenom);

-- 5 : Tri + limitation (avec ROWNUM)

	-- 5.1 - Question : Quels sont les 3 noms de familles des élèves qui s'appellent Thomas dans l'ordre alphabétique ?

	-- Réponse en algèbre relationnelle :

	-- Eleve{prenom = 'Thomas' & ROWNUM <= 3}[nom](nom>)

	-- Réponse en SQL :

	SELECT *
	FROM (
		SELECT DISTINCT UPPER(nom)
		FROM Eleve
		WHERE prenom = 'Thomas'
		ORDER BY UPPER(nom)
	)
	WHERE ROWNUM <= 3;

-- 6 : Jointure de 2 tables 

	-- 6.1 - Question : Quelles sont les filières et les mentions qui leur sont associées ?

	-- Réponse en algèbre relationnelle :

	-- Filiere*Mention

	-- Réponse en SQL :

	SELECT *
	FROM Filiere, Mention
	WHERE nomFiliere = uneFiliere;

	-- 6.2 - Question : Quels sont les etablissements et les voeux qui leur sont associés ?

	-- Réponse en algèbre relationnelle :

	-- Etablissement*Voeu

	-- Réponse en SQL :

	SELECT *
	FROM Etablissement, Voeu
	WHERE numEtablissement = unEtablissementVoeu;

-- 7 : Jointure de 3 tables

	-- 7.1 - Question : Afficher pour chaque voeu son resultat, sa mention, sa filière, si elle est séléctive, le numero de l'eleve qui la formulé et l'établissement dans lequel il l'a formulé.

	-- Réponse en algèbre relationnelle :

	-- Voeu*Mention*Filiere[numeroVoeu, resultat, intituleMention, nomFiliere, estSelective, unEleve, unEtablissementVoeu]

	-- Réponse en SQL :

	SELECT DISTINCT numeroVoeu, UPPER(resultat), UPPER(intituleMention), UPPER(nomFiliere), estSelective, unEleve, unEtablissementVoeu
	FROM Voeu, Mention, Filiere
	WHERE uneMentionVoeu = intituleMention
	AND uneFiliereVoeu = nomFiliere;

-- 8 : Auto-jointure 

	-- 8.1 - Question :

    -- afficher tous les noms des élèves qui apparaisse deux fois

    -- Réponse en algèbre relationnelle :

    -- (Eleve E1[[E1.nom=E2.nom]]Eleve E2){E1.numeroINE != E2.numeroINE}[E1.nom, E1.prenom])

    -- Réponse en SQL :

    SELECT DISTINCT UPPER(E1.nom), UPPER(E1.prenom)
    FROM Eleve E1, Eleve E2
    WHERE UPPER(E1.nom) = UPPER(E2.nom)
    AND UPPER(E1.numeroINE) != UPPER(E2.numeroINE)
